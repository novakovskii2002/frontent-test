export class Property {
  constructor() {
    this.point = [0, 0];
    this.media = [];
  }

  id: number;
  roomsCount: number;
  square: number;
  description: string;
  address: string;
  point: number[];
  media: string[];
}
