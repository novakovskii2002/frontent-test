import {Component, Input, OnInit} from '@angular/core';
import {PropertyService} from '../property.service';
import {Property} from '../property';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {PropertyMapComponent} from '../property-list/property-map.component';

@Component({
  selector: 'app-property-details',
  templateUrl: './property-details.component.html',
  styleUrls: ['./property-details.component.css']
})
export class PropertyDetailsComponent implements OnInit {
  @Input() property: Property;
  @Input() readonlyPoint = false;
  propertyMapComponent: PropertyMapComponent;
  currentFile: File;
  @Input() deletable = true;

  constructor(private propertyService: PropertyService,
              public activeModal: NgbActiveModal) {
  }

  ngOnInit(): void {
  }

  save() {
    console.log(this.property);
    this.propertyService.save(this.property)
      .subscribe(_ => this.refresh());
  }

  delete() {
    this.propertyService.delete(this.property.id)
      .subscribe(_ => this.refresh());
  }

  refresh() {
    if (this.propertyMapComponent != null) {
      this.propertyMapComponent.refresh();
      this.activeModal.close();
    }
  }

  uploadMedia() {
    this.propertyService
      .saveMedia(this.currentFile)
      .subscribe(name => {
        console.log(name);
        this.property.media.push(name);
      });
  }

  onFileChange(event: Event) {
    const target = event.target as HTMLInputElement;
    this.currentFile = target.files[0];
    target.name = this.currentFile.name;
  }
}
