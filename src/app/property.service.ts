import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Property} from './property';
import {LatLngBounds} from '@agm/core';

const options = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

const url = 'http://localhost:8080/api';

@Injectable({
  providedIn: 'root'
})
export class PropertyService {
  constructor(private http: HttpClient) {
  }

  getAllPins(): Observable<Property[]> {
    return this.http.get<Property[]>(`${url}/property/all`);
  }

  getById(id: number): Observable<Property> {
    return this.http.get<Property>(`${url}/property/id/${id}`);
  }

  getInBounds(bounds: LatLngBounds): Observable<Property[]> {
    return this.http.get<Property[]>(`${url}/property/area`, {
      headers: {
        'Content-Type': 'application/json'
      },
      params: {
        'leftUpperPoint': `[${bounds.getNorthEast().lat()}, ${bounds.getNorthEast().lng()}]`,
        'rightBottomPoint': `[${bounds.getSouthWest().lat()}, ${bounds.getSouthWest().lng()}]`
      }
    });
  }

  saveMedia(file: File): Observable<string> {
    const formData = new FormData();
    console.log(file);
    formData.append('file', file);

    return this.http.post(`${url}/media/upload`, formData, {
      responseType: 'text'
    });
  }

  save(property: Property) {
    return this.http.post(`${url}/property`, {
      id: property.id,
      description: property.description,
      address: property.address,
      roomsCount: property.roomsCount,
      square: property.square,
      point: [property.point[0], property.point[1]],
      media: property.media
    }, options);
  }

  delete(id: number) {
    return this.http.delete(`${url}/property`, {
      params: {
        id: id.toString()
      }
    });
  }
}
