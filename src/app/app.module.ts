import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {PropertyMapComponent} from './property-list/property-map.component';
import {HttpClientModule} from '@angular/common/http';
import {AgmCoreModule} from '@agm/core';
import {PropertyDetailsComponent} from './property-details/property-details.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PropertyAddressComponent } from './property-address/property-address.component';


@NgModule({
  declarations: [
    AppComponent,
    PropertyMapComponent,
    PropertyDetailsComponent,
    PropertyAddressComponent
  ],

  imports: [
    BrowserModule,
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBieUckjdkdlGMYiQcmdKZd3hg8dOnXRhg',
      libraries: ['places']
    }),
    FormsModule,
    BrowserAnimationsModule,
    NgbModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
