import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {Property} from '../property';
import {PropertyService} from '../property.service';
import {AgmMap, LatLngBounds, MouseEvent} from '@agm/core';
import {PropertyDetailsComponent} from '../property-details/property-details.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {PropertyAddressComponent} from '../property-address/property-address.component';

@Component({
  selector: 'app-property-map',
  templateUrl: './property-map.component.html',
  styleUrls: ['./property-map.component.css']
})
export class PropertyMapComponent implements OnInit {

  properties: Property[];
  private lastBounds: LatLngBounds;
  @ViewChild('map') map: AgmMap;

  constructor(private propertyService: PropertyService,
              private ngbModal: NgbModal) {
  }

  ngOnInit(): void {
  }

  @HostListener('document:contextmenu', ['$event'])
  onRightClick(event) {
    event.preventDefault();
  }

  onMarkerClick(property: Property) {
    const modal = this.ngbModal.open(PropertyDetailsComponent);

    modal.componentInstance.property = property;
    modal.componentInstance.propertyMapComponent = this;
  }

  onBoundsChanged(event: LatLngBounds) {
    this.lastBounds = event;

    this.propertyService.getInBounds(event)
      .subscribe(properties => {
        console.log(properties);
        this.properties = properties;
      });
  }

  onRightMapClick(event: MouseEvent) {
    const property = new Property();
    const modal = this.ngbModal.open(PropertyDetailsComponent);

    property.point[0] = event.coords.lat;
    property.point[1] = event.coords.lng;

    modal.componentInstance.property = property;
    modal.componentInstance.readonlyPoint = true;
    modal.componentInstance.deletable = false;
    modal.componentInstance.propertyMapComponent = this;

    modal.result.then(_ => this.refresh());
  }

  openAddress() {
    const modal = this.ngbModal.open(PropertyAddressComponent);
    modal.componentInstance.map = this.map;
  }

  refresh() {
    if (this.lastBounds != null) {
      this.propertyService.getInBounds(this.lastBounds)
        .subscribe(properties => this.properties = properties);
    }
  }
}
