import {Component, ElementRef, Input, NgZone, OnInit, ViewChild} from '@angular/core';
import {Property} from '../property';
import {PropertyDetailsComponent} from '../property-details/property-details.component';
import {AgmMap, MapsAPILoader} from '@agm/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-property-address',
  templateUrl: './property-address.component.html',
  styleUrls: ['./property-address.component.css']
})
export class PropertyAddressComponent implements OnInit {
  @ViewChild('search') searchElementRef: ElementRef;
  @Input() map: AgmMap;

  constructor(private mapsAPILoader: MapsAPILoader,
              private ngZone: NgZone,
              private ngbModal: NgbModal) {
  }

  ngOnInit(): void {
    this.findAddress();
  }

  changeIndex(){
    const element = document.querySelector('.pac-container') as HTMLElement;
    element.style.zIndex = '999999';
  }

  findAddress() {
    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          const result = autocomplete.getPlace();
          this.map.latitude = result.geometry.location.lat();
          this.map.longitude = result.geometry.location.lng();

          const property = new Property();
          const modal = this.ngbModal.open(PropertyDetailsComponent);

          property.point[0] = result.geometry.location.lat();
          property.point[1] = result.geometry.location.lng();


          modal.componentInstance.property = property;
          modal.componentInstance.readonlyPoint = true;
          modal.componentInstance.deletable = false;
          modal.componentInstance.propertyMapComponent = this;
        });
      });
    });
  }

}
